# Info-Portal zur Entwicklung barrierefreier Apps
Das Info-Portal ist eine Projekt Arbeit im Rahmen des Studiengangs 
Medieninformatik an der Technischen Hochschule Mittelhessen f&uuml;r das Modul
Software Ergonomie im Wintersemester 2018/2019.<br>
Es bietet allgemeine Informationen, ein Wiki, sowie eine interaktive Checkliste
mit wichtigen Richtlininien zur Entwicklung einer barrierefreien App.

<br>

## Installation
Das Portal verwendet Django und Foundation CSS als Frameworks. N&ouml;tige Pakete
k&ouml;nnen mit pip (im project root) und npm (im static Verzeichnis) installiert werden.
Zudem wird eine PostgresSQL Datenbank ben&ouml;tigt.<br>
Nachdem alle Abh&auml;ngigkeiten installiert wurden, kann der Server mit dem Befehl
`python3 manage.py runserver` im project root gestartet werden und unter der Adresse
[127.0.0.1:8000](https://127.0.0.1:8000) im Browser aufgerufen werden.

<br>

## Dokumentation
Relevanter Code ist in den jeweiligen nicht Framework Dateien kommentiert.
Zu diesen Dateien geh&ouml;ren:
* */views.py
* */models.py
* */templates/\*/\*.html
* /static/js/app.js
