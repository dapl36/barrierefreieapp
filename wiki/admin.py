from django.contrib import admin

from .models import WikiEntry, WikiExample

admin.site.register(WikiEntry)
admin.site.register(WikiExample)
