from django.db import models


class WikiEntry(models.Model):
    """
    Model for wiki entries.
    allows queries for negative and positive examples.
    """
    wikiTitle = models.CharField(max_length=200)
    wikiText = models.TextField()

    def getPositiveExamples(self):
        return self.wikiexample_set.filter(examplePositive=True)

    def getNegativeExamples(self):
        return self.wikiexample_set.filter(examplePositive=False)

    def __str__(self):
        return self.wikiTitle

    class Meta:
        ordering = ['wikiTitle']


class WikiExample(models.Model):
    """
    Model for wiki example to be used for an wiki entry.
    """
    wikiEntry = models.ForeignKey(
        WikiEntry, on_delete=models.SET_NULL, null=True)
    exampleTitle = models.CharField(max_length=200)
    exampleText = models.TextField(default="Beispiel")
    exampleImage = models.CharField(max_length=200)
    exampleImageAlt = models.CharField(max_length=400, default="Bild")
    examplePositive = models.BooleanField(default=True)

    def __str__(self):
        return self.exampleTitle

    class Meta:
        ordering = ['-examplePositive', 'exampleTitle']
