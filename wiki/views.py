from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.db.models import Prefetch
from django.views import generic

from .models import  WikiEntry, WikiExample


class IndexView(generic.ListView):
    """
    Default view for wiki page.
    Passes data from wiki models to wiki template.
    """
    template_name = 'wiki/index.html'
    context_object_name = 'wiki_list'

    def get_queryset(self):
        return WikiEntry.objects.all()

