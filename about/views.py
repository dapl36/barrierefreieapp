from django.views import generic


class IndexView(generic.TemplateView):
    """
    Default view for about page
    """
    template_name = 'about/index.html'


class ImpressumView(generic.TemplateView):
    """
    View class that loads the impressum
    """
    template_name = 'about/impressum.html'
