from django.views import generic


class IndexView(generic.TemplateView):
    """
    Default view for welcome page
    """
    template_name = 'welcome/index.html'
