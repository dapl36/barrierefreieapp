from django.contrib import admin

from .models import CheckListEntry, CheckListCategory

admin.site.register(CheckListEntry)
admin.site.register(CheckListCategory)
