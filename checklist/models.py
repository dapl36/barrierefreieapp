from django.db import models


class CheckListCategory(models.Model):
    """
    Model for checklist categories
    """
    wikiEntry = models.ForeignKey(
        'wiki.WikiEntry', on_delete=models.SET_NULL, blank=True, null=True)
    categoryTitle = models.CharField(max_length=100)

    def getEntries(self):
        return self.checklistentry_set.all()

    def __str__(self):
        return self.categoryTitle


class CheckListEntry(models.Model):
    """
    Model for checklist entries, severity describes the importance of an entry
    """
    checklistCategory = models.ForeignKey(
        'checklist.CheckListCategory', on_delete=models.SET_DEFAULT, default=1)
    wikiEntry = models.ForeignKey(
        'wiki.WikiEntry', on_delete=models.SET_NULL, blank=True, null=True)
    checkListEntryTitle = models.CharField(max_length=200)
    checkListEntrySeverity = models.CharField(max_length=10)
    checkListEntryDescription = models.TextField(blank=True, null=True)

    def __str__(self):
        return self.checkListEntryTitle

    class Meta:
        ordering = ['-checkListEntrySeverity', 'checkListEntryTitle']
