from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.views import generic

from .models import CheckListCategory


class IndexView(generic.ListView):
    """
    Default view for checklist page.
    Passes date from models to checklist template.
    """
    template_name = 'checklist/index.html'
    context_object_name = 'checklist'

    def get_queryset(self):
        return CheckListCategory.objects.all()

