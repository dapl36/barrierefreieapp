# Generated by Django 2.1.3 on 2018-12-17 18:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('wiki', '0001_initial'),
        ('checklist', '0006_auto_20181217_1833'),
    ]

    operations = [
        migrations.AddField(
            model_name='checklistcategory',
            name='wikiEntry',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='wiki.WikiEntry'),
        ),
    ]
