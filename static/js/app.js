$(document).foundation();

// Call loadLocalStorage() on page load, to load existing data from local
// storage
document.addEventListener("DOMContentLoaded", function(event) {
    loadLocalStorage();
});

// Json object to store checklist entries by their id
var checklistJson = {"entries" : {"entry_1": true,}};

// Toggles the state of a checklist entry on button click
function checkListToggle(element, checklistId) {
    let completed = element.classList.contains("alert") ? true : false;
    element.innerHTML = completed ? "Abgeschlossen" : "Offen";
    element.classList.toggle("alert");
    element.classList.toggle("success");
    updateLocalStorage(checklistId, completed);
}

// Updates local storage and adds, or changes, the state of an checklist entry
// within the local storage
function updateLocalStorage(checklistId, completed) {
    let checklistJson = JSON.parse(localStorage.getItem('checklist'))
    if (checklistJson === null) {
        checklistJson = {};
    }
    let item = "entry_" + checklistId;
    checklistJson[item] = completed;
    localStorage.setItem('checklist', JSON.stringify(checklistJson));
}

// Checks local storage for existing checklist state.
// If a checklist entry was previously marked as complete, it gets completed
// automatically on page load to allow continious usage even if the tab
// or browser was closed.
function loadLocalStorage() {
    let checklistJson = JSON.parse(localStorage.getItem('checklist'))
    console.log(checklistJson);
    if (!document.getElementById("checklist")) return;
    for (let item in checklistJson) {
        let button = document.getElementById(item).getElementsByTagName("button")[0];
        if (button) {
            if (checklistJson[item]) {
                button.innerHTML = checklistJson[item] ? "Abgeschlossen" : "Offen";
                button.classList.toggle("alert");
                button.classList.toggle("success");
            }
        }
    }
}
